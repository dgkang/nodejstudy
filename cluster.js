var cluster = require("cluster"),os = require("os");

var cpuNums = os.cpus().length;

var workers = {};

if(cluster.isMaster){
	cluster.on("exit",function(worker,code,signal){
		console.log("Process death,pid: %d,code: %d,signal: %d",worker.process.pid,code,signal);
		delete workers[worker.id];
		worker = cluster.fork();
		workers[worker.pid] = worker;
	});

	if(process.argv[2] != undefined)
		cpuNums = process.argv[2];
	for (var i = 0; i < cpuNums; i++){
		worker = cluster.fork();
		workers[worker.id] = worker;
	}

	process.on("SIGTERM",function(){
		for (var id in workers){
			console.log("kill pid: %d",workers[id].process.pid);
			process.kill(workers[id].process.pid);
		}
		process.exit(0);
	});

}else{
	var app = require("./app");
	app.listen(8080);
}

	
