var http = require("http");
var app = http.createServer(function(req,res){
	res.writeHead("200");
	var str = "Hello World,PID: " + process.pid;
	res.write(str);
	res.end();
});

app.listen(8080);

module.exports = app;
